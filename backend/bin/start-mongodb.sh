dbPathDoesNotExist() {
  if [ ! -d $1 ]; then
    return 0
  else
    return 1
  fi
}

dbPath=$1

if [ -z $dbPath ]; then
  echo "Usage: start_mongodb data-directory-path"
  exit 1
fi

dbPathDoesNotExist $dbPath
shouldMakeDbPath=$?

if [ $shouldMakeDbPath -eq 0 ]; then
  echo "Creating data directory: '$dbPath'"
  mkdir $dbPath

  dbPathDoesNotExist $dbPath
  shouldMakeDbPath=$?

  if [ $shouldMakeDbPath -eq 0 ]; then
    echo "No DB path"
    exit 1
  fi
fi

echo "Starting mongodb with db path: '$dbPath'"
echo "###############"

mongod --dbpath $dbPath

exit 0