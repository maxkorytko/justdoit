/**
 * Created by maxk on 2014-06-04.
 */

'use strict';

var TodoModel = {
    create : function(database) {
        var todoSchema = {title : String, done : Boolean, createdOn : Number};
        return database.createModel('Todos', todoSchema);
    }
};

module.exports = TodoModel;