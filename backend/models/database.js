/**
 * Created by maxk on 2014-06-04.
 */

'use strict';

var mongoose = require('mongoose');

function Database() {
    this.host = 'mongodb://localhost/JustDoIt';
}

Database.prototype = {
    constructor : Database,

    connect : function(callback) {
        mongoose.connect(this.host);

        var connection = mongoose.connection;

        connection.on('error', function() {
            callback('error connecting to the database');
        });

        connection.on('open', function() {
            callback(null);
        });
    },

    createModel : function(name, schema) {
        return mongoose.model(name, mongoose.Schema(schema));
    }
}

module.exports = new Database();