# A simple Node.JS app that exposes a REST API to manage a list of todo items #

# Requirements #

* Node.js
* Mongo DB

Ensure Node.js and Mongo DB executables are in your PATH.

# How to run #

* npm install # do this once
* bin/start_db.sh data
* npm start
