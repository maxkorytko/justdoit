'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var database = require('../models/database');
var TodoModel = require('../models/todo-model');

var Todo;
var jsonParser = bodyParser.json();
var router = express.Router();

database.connect(function(error) {
    if (error) {
        console.error(error);
        return;
    }

    createModels();
});

function createModels() {
    Todo = TodoModel.create(database);
}
/**
 * Queries the database for items filtered by the provided filter.
 * @returns A list of items.
 */
router.get('/:filter?', function(req, res) {
    createItemsQuery(req.params.filter).exec(function(error, todos) {
        res.json(error ? error : buildItemsListResponse(todos));
    });
    /**
     * Creates a query object based on the specified filter.
     * The query can be used to return a subset of todo items.
     * @returns A query object.
     */
    function createItemsQuery(filter) {
        return Todo.where('done', filter === 'done');
    }
    /**
     * Transforms the given list of todo items into another list.
     * Each element in the transformed list contains additional details about an item.
     * @param todos
     * @returns {Array}
     */
    function buildItemsListResponse(todos) {
        return todos.map(responseItemFromItem); 
    }
});
/**
 * Creates a new object from the specified todo object by filtering out unwanted properties (e.g. id).
 * The newly created object is a todo item that's suitable for sending in the response to the client.
 */
function responseItemFromItem(todo) {
    var item = {};

    if (todo) {
        item = {
            id          : todo._id,
            title       : todo.title,
            done        : todo.done,
            createdOn   : todo.createdOn,
        };
    }

    return item;
}
/**
 * Constructs a todo item from the provided parameters.
 * Saves the item to the database.
 * @returns A JSON object containing an error and item objects.
 */
router.post('/create', function(req, res) {
    var todo = new Todo({
        title : req.body.title,
        done : false,
        createdOn : req.body.createdOn ? parseInt(req.body.createdOn) : new Date().getTime()
    });

    todo.save(function(error, todo) {
        res.json({'error':error, 'item':responseItemFromItem(todo)});
    });
});
/**
 * Accepts a JSON array of items to update.
 * Queries the database for these items and updates each one.
 * @returns A JSON object containing an error and items objects.
 */
router.put('/update', jsonParser, function(req, res) {
    var itemsJson = req.body;

    if (!Array.isArray(itemsJson)) {
        res.status(400).send("Must provide a JSON array in the request body");
        return;
    }

    Todo.find({'_id' : {$in : itemsJson.map(function(item){ return item.id })}}, updateItems);

    function updateItems(error, items) {
        if (error) {
            sendResponse(error, items);
            return;
        }

        var saveError;

        items.forEach(function(item, index) {
            if (!saveError) {
                updateItem(item, itemsJson[index]);
                item.save(function(error, item, numberAffected) {
                    saveError = error;

                    if (index == items.length - 1) {
                        sendResponse(saveError, items);
                    }
                });
            }
        });
    }

    function updateItem(item, values) {
        if (values.title) item.title = values.title;
        if (values.done) item.done = values.done;
    }

    function sendResponse(error, todos) {
        res.json({'error':error, 'items':todos.map(responseItemFromItem)});
    }
});
/**
 * Deletes an item from the database.
 * @returns A JSON object containing an error object.
 */
router.delete('/:itemId/delete', function(req, res) {
    var id = req.params.itemId;

    Todo.findByIdAndRemove(id, function(error) {
        res.json({'error':error});
    });
});

module.exports = router;
