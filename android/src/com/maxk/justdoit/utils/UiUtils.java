package com.maxk.justdoit.utils;

import android.content.Context;
import android.widget.Toast;

import com.maxk.justdoit.application.JustDoItApplication;

/**
 * Implements various utility methods for working with the UI.
 * @author MaxK
 */
public class UiUtils {
    /**
     * Determines if the app is running on a phone or tablet.
     * @param context
     * @return
     */
	public static boolean isTablet(Context context) {
		return context.getResources().getConfiguration().smallestScreenWidthDp >= 600;
	}

    /**
     * Convenience wrapper around standard toast API.
     * Allows displaying toasts in one line of code.
     * @param stringId
     */
    public static void showToast(int stringId) {
        Toast.makeText(JustDoItApplication.getContext(),
                stringId,
                Toast.LENGTH_SHORT)
                .show();
    }
}
