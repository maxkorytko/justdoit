package com.maxk.justdoit.controller;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.maxk.justdoit.R;
import com.maxk.justdoit.api.JustDoItApiJsonRequest;
import com.maxk.justdoit.api.JustDoItApiRequest;
import com.maxk.justdoit.model.CreateTodoItemResult;
import com.maxk.justdoit.model.TodoItem;
import com.maxk.justdoit.model.UpdateTodoItemResult;
import com.maxk.justdoit.view.TodoItemsView;

/**
 * Controller for the to-do items behavior.
 * @author MaxK
 */
public class TodoItemsController {
	private final TodoItemsView _view;
	
	public TodoItemsController(TodoItemsView view) {
		if (view == null) throw new IllegalArgumentException("view");
		
		_view = view;
	}

    /**
     * Loads uncompleted to-do items from the backend.
     * This method is non-blocking.
     * The controller notifies the view once the operation is complete.
     */
	public void loadTodoItems() {
		loadItems("todo");
	}

    /**
     * Loads completed to-do items from the backend.
     * This method is non-blocking.
     * The controller notifies the view once the operation is complete.
     */
	public void loadDoneItems() {
		loadItems("done");
	}

    private void loadItems(String filter) {
        loadItems(filter, new TodoItem.RequestStatusListener<TodoItem[]>() {
            @Override
            public void onRequestSuccess(TodoItem[] items) {
                _view.showItems(items);
            }

            @Override
            public void onRequestFail() {
                _view.showItems(new TodoItem[0]);
            }
        });
    }

	private static void loadItems(String filter, final TodoItem.RequestStatusListener<TodoItem[]> listener) {
        JustDoItApiRequest.itemsListRequest(filter,
                new Response.Listener<TodoItem[]>() {
                    @Override
                    public void onResponse(TodoItem[] items) {
                        if (listener != null) listener.onRequestSuccess(items);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (listener != null) listener.onRequestFail();
                    }
                }).execute();
	}

    /**
     * Creates the item on the back-end.
     * This method is non blocking.
     * The controller notifies the view once the operation is complete.
     * @param item
     */
    public void createTodoItem(TodoItem item) {
        if (item == null) return;

        createTodoItem(item, new TodoItem.RequestStatusListener<TodoItem>() {
            @Override
            public void onRequestSuccess(TodoItem savedItem) {
                _view.showItem(savedItem, R.string.todo_item_saved);
            }

            @Override
            public void onRequestFail() {
                _view.showErrorMessage(R.string.todo_item_save_error);
            }
        });
    }

    private static void createTodoItem(TodoItem item, final TodoItem.RequestStatusListener<TodoItem> listener) {
        if (item == null) throw new IllegalArgumentException("item");

        JustDoItApiRequest<CreateTodoItemResult> createRequest = JustDoItApiRequest.createTodoItemRequest(item,
                new Response.Listener<CreateTodoItemResult>() {
                    @Override
                    public void onResponse(CreateTodoItemResult response) {
                        if (listener != null) listener.onRequestSuccess(response.item);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (listener != null) listener.onRequestFail();
                    }
                });

        createRequest.execute();
    }

    /**
     * Marks specified to-do items as complete and updates them on the back-end.
     * This method is non-blocking.
     * The controller notifies the view once the operation is complete.
     * @param items
     */
    public void completeTodoItems(final TodoItem[] items) {
        for (TodoItem item : items) {
            item.done = true;
        }

        updateTodoItems(items, new TodoItem.RequestStatusListener<TodoItem[]>() {
            @Override
            public void onRequestSuccess(TodoItem[] result) {
                // nothing
            }

            @Override
            public void onRequestFail() {
                for (TodoItem item : items) {
                    item.done = false;
                }

                int messageId = items.length > 1 ? R.string.todo_items_complete_error
                                                 : R.string.todo_item_complete_error;

                _view.revertUpdateTodoItems(items);
                _view.showErrorMessage(messageId);
            }
        });
    }

    private static void updateTodoItems(TodoItem[] items, final TodoItem.RequestStatusListener<TodoItem[]> listener) {
        JustDoItApiRequest<UpdateTodoItemResult> updateRequest = JustDoItApiJsonRequest.updateTodoItemsRequest(items,
                new Response.Listener<UpdateTodoItemResult>() {
                    @Override
                    public void onResponse(UpdateTodoItemResult response) {
                        if (listener != null) {
                            if (response.error != null) listener.onRequestFail();
                            else listener.onRequestSuccess(response.items);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (listener != null) listener.onRequestFail();
                    }
                });

        updateRequest.execute();
    }

    /**
     * Deletes to-do items on the back-end.
     * This method is non-blocking.
     * The controller notifies the view when the operation is complete.
     * @param items
     */
    public void deleteTodoItems(final TodoItem[] items) {
        if (items == null || items.length == 0) return;

        deleteTodoItems(items, new TodoItem.RequestStatusListener<Void>() {
            @Override
            public void onRequestSuccess(Void result) {
                // nothing
            }

            @Override
            public void onRequestFail() {
                int messageId = items.length > 1 ? R.string.todo_items_delete_error
                                                 : R.string.todo_item_delete_error;

                _view.showErrorMessage(messageId);
            }
        });
    }

    private static void deleteTodoItems(TodoItem[] items, final TodoItem.RequestStatusListener<Void> listener) {
        JustDoItApiRequest.deleteTodoItemsRequest(items, new Response.Listener<Void>() {
                    @Override
                    public void onResponse(Void response) {
                        if (listener != null) listener.onRequestSuccess(null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (listener != null) listener.onRequestFail();
                    }
                }).execute();
    }
}
