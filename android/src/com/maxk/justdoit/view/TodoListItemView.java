package com.maxk.justdoit.view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.maxk.justdoit.R;
import com.maxk.justdoit.model.TodoItem;

/**
 * Compound view representing a to-do item inside a list view.
 * @author maxk
 *
 */
@SuppressLint("ViewConstructor")
public class TodoListItemView extends FrameLayout {
	public static interface ActionViewFactory {
		/**
		 * Creates an action view, which will be displayed when the user swipes from right to left.
		 * @param context Context.
		 * @param position Position of the view in the list view.
		 */
		View createRightActionView(Context context, int position);
		/**
		 * Creates an action view, which will be displayed when the user swipes from left to right.
		 * @param context Context.
		 * @param position Position of the view in the list view.
		 */
		View createLeftActionView(Context context, int position);
	}
	
	/**
	 * Container for all child views.
	 */
	private static final class ViewHolder {
		public final ViewGroup topView;
		public final ViewGroup bottomView;
		public final TextView title;
		public final TextView subtitle; 
		
		public ViewHolder(TodoListItemView view) {
			topView = (ViewGroup)view.findViewById(R.id.topView);
			bottomView = (ViewGroup)view.findViewById(R.id.bottomView);
			title = (TextView)view.findViewById(R.id.titleTextView);
			subtitle = (TextView)view.findViewById(R.id.subtitleTextView);
		}
	}
	
	private static long ANIMATION_DURATION = 250;
	
	private final int _position;
	private final ActionViewFactory _actionViewFactory;
	private final ViewHolder _viewHolder;
	private View _rightActionView;
	private View _leftActionView;
	
	public TodoListItemView(Context context, int position, ActionViewFactory actionViewFactory) {
		super(context);
		
		if (actionViewFactory == null) throw new IllegalArgumentException("actionViewFactory");
		
		_position = position;
		_actionViewFactory = actionViewFactory;
		LayoutInflater.from(context).inflate(R.layout.todo_list_item_layout, this);
		_viewHolder = new ViewHolder(this);
		
		addActionViews();
		attachGestureDetector();
	}
	
	private void addActionViews() {
		addActionView(getRightActionView());
		addActionView(getLeftActionView());
	}
	
	private void addActionView(View actionView) {
		if (actionView == null) return;
		
		_viewHolder.bottomView.addView(actionView);
	}
	
	private View getRightActionView() {
		if (_rightActionView == null) {
			_rightActionView = _actionViewFactory.createRightActionView(getContext(), _position);
			
			if (_rightActionView != null) {
				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				
				_rightActionView.setLayoutParams(layoutParams);
			}
		}
		
		return _rightActionView;
	}
	
	private View getLeftActionView() {
		if (_leftActionView == null) {
			_leftActionView = _actionViewFactory.createLeftActionView(getContext(), _position);
			
			if (_leftActionView != null) {
				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				
				_leftActionView.setLayoutParams(layoutParams);
			}
		}
		
		return _leftActionView;
	}
	
	private void attachGestureDetector() {
		final GestureDetector gestureDetector = new GestureDetector(getContext(),
				new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}
			
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				if (_viewHolder.topView.getTranslationX() != 0) {
					hideActionView();
				} else {
					revealActionView(velocityX);
				}
				
				return true;
			}
		});
		
		setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		});
	}
	
	private void hideActionView() {		
		AnimatorSet translationAnimation = new AnimatorSet();
		
		translationAnimation.play(ObjectAnimator.ofFloat(_viewHolder.topView, "translationX", 0f))
							.with(ObjectAnimator.ofFloat(_viewHolder.title, "translationX", 0f))
							.with(ObjectAnimator.ofFloat(_viewHolder.subtitle, "translationX", 0f));
		
		translationAnimation.setDuration(ANIMATION_DURATION);
		translationAnimation.start();
	}
	
	private void revealActionView(float velocity) {
		if (velocity > 0) revealLeftActionView();
		else revealRightActionView();
	}
	
	private void revealLeftActionView() {
		if (_leftActionView == null) return;
		
		final float delta = _leftActionView.getWidth() + _viewHolder.bottomView.getPaddingLeft();
		_viewHolder.topView.animate().setDuration(ANIMATION_DURATION).translationXBy(delta);
	}
	
	private void revealRightActionView() {
		if (_rightActionView == null) return;
		
		final float delta = _rightActionView.getWidth() + _viewHolder.bottomView.getPaddingRight();
		
		AnimatorSet translationAnimation = new AnimatorSet();
		
		translationAnimation.play(ObjectAnimator.ofFloat(_viewHolder.topView, "translationX", -delta))
							.with(ObjectAnimator.ofFloat(_viewHolder.title, "translationX", delta))
							.with(ObjectAnimator.ofFloat(_viewHolder.subtitle, "translationX", delta));
		
		translationAnimation.setDuration(ANIMATION_DURATION);
		translationAnimation.start();
	}
	
	public void render(TodoItem item) {
		if (item == null) throw new IllegalArgumentException("item");
		
		_viewHolder.topView.setTranslationX(0);
		_viewHolder.title.setTranslationX(0);
		_viewHolder.subtitle.setTranslationX(0);
		_viewHolder.title.setText(item.title);
		_viewHolder.subtitle.setText(item.formatCreatedOnDate());
	}
}
