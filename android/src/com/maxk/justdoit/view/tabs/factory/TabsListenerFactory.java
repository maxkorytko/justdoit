package com.maxk.justdoit.view.tabs.factory;

import android.app.Activity;
import android.content.Context;

import com.maxk.justdoit.utils.UiUtils;
import com.maxk.justdoit.view.fragment.DoneItemsListFragment;
import com.maxk.justdoit.view.fragment.DoneItemsGridFragment;
import com.maxk.justdoit.view.fragment.TodoItemsListFragment;
import com.maxk.justdoit.view.fragment.TodoItemsGridFragment;
import com.maxk.justdoit.view.tabs.TabListener;

/**
 * Factory for tab listeners.
 * @author MaxK
 */
public abstract class TabsListenerFactory {
	public static TabsListenerFactory create(Context context) {
		if (UiUtils.isTablet(context)) {
			return new TabletTabsListenerFactory();
		}
		
		return new PhoneTabsListenerFactory();
	}
	
	public abstract TabListener<?> todoItemsTabListener(Activity activity);
	public abstract TabListener<?> doneItemsTabListener(Activity activity);

    /**
     * Phone specific tabs listener factory.
     */
	private static class PhoneTabsListenerFactory extends TabsListenerFactory {
		public TabListener<TodoItemsListFragment> todoItemsTabListener(Activity activity) {
			return new TabListener<TodoItemsListFragment>(activity,
					TodoItemsListFragment.TAG, TodoItemsListFragment.class);
		}
		
		public TabListener<DoneItemsListFragment> doneItemsTabListener(Activity activity) {
			return new TabListener<DoneItemsListFragment>(activity,
					DoneItemsListFragment.TAG, DoneItemsListFragment.class);
		}
	}

    /**
     * Tablet specific tabs listener factory.
     */
	private static class TabletTabsListenerFactory extends TabsListenerFactory {
		public TabListener<TodoItemsGridFragment> todoItemsTabListener(Activity activity) {
			return new TabListener<TodoItemsGridFragment>(activity,
					TodoItemsGridFragment.TAG, TodoItemsGridFragment.class);
		}
		
		public TabListener<DoneItemsGridFragment> doneItemsTabListener(Activity activity) {
			return new TabListener<DoneItemsGridFragment>(activity,
					DoneItemsGridFragment.TAG, DoneItemsGridFragment.class);
		}
	}
}
