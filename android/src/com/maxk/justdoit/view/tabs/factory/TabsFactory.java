package com.maxk.justdoit.view.tabs.factory;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;

import com.maxk.justdoit.R;

/**
 * Creates tabs for individual activities.
 * @author maxk
 * 
 */
public class TabsFactory {
	private final Activity _activity;
	private final ActionBar _actionBar;
	
	public TabsFactory(Activity activity) {
		if (activity == null) throw new IllegalArgumentException("activity");
		
		_activity = activity;
		_actionBar = activity.getActionBar();
	}
	
	/**
	 * Creates tabs that are displayed on the TodosActivity.
	 * @return
	 */
	public Tab[] makeTabsForTodosActivity() {
		if (_actionBar == null) return getEmptyTabs();
		
		Tab[] tabs = new Tab[2];
		TabsListenerFactory listenerFactory = TabsListenerFactory.create(_activity);
		
		tabs[0] = _actionBar.newTab()
					.setText(R.string.todo_items_tab_title)
					.setTabListener(listenerFactory.todoItemsTabListener(_activity));
		
		tabs[1] = _actionBar.newTab()
					.setText(R.string.done_items_tab_title)
					.setTabListener(listenerFactory.doneItemsTabListener(_activity));
		
		return tabs;
	}
	
	private static Tab[] getEmptyTabs() {
		return new Tab[]{};
	}	
}
