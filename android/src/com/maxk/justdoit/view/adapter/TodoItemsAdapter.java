package com.maxk.justdoit.view.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.maxk.justdoit.model.TodoItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Adapter for the to-do items collection.
 * @author MaxK
 *
 */
public class TodoItemsAdapter extends BaseAdapter {
	/**
	 * A view provider provides a view for each to-do item.
	 * @author maxk
	 *
	 */
	public static interface ViewProvider {
		View getTodoItemView(View convertView, ViewGroup parent, int position);
	}
	
	private final List<TodoItem> _todoItems;
	private final ViewProvider _viewProvider;
	
	public TodoItemsAdapter(TodoItem[] items, ViewProvider viewProvider) {
        if (items == null) throw new IllegalArgumentException("items");
		if (viewProvider == null) throw new IllegalArgumentException("viewProvider");
		
		_todoItems = new ArrayList<TodoItem>(Arrays.asList(items));
		_viewProvider = viewProvider;
	}
	
	@Override
	public int getCount() {
		return _todoItems.size();
	}

	@Override
	public Object getItem(int position) {
		return getTodoItem(position);
	}
	
	/**
	 * Finds a to-do item at the specified position.
	 * @param position
	 * @return A to-do item or null if no item at the specified position.
	 */
	public TodoItem getTodoItem(int position) {
		if (position < 0 || position >= _todoItems.size()) return null;
		
		return _todoItems.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return _viewProvider.getTodoItemView(convertView, parent, position);
	}
	
	/**
	 * Adds a given to-do item to the underlying to-do items collection.
	 * Notifies that collection has changed.
	 * @param item
	 */
	public void addTodoItem(TodoItem item) {
		if (item == null) return;
		
		addTodoItems(new TodoItem[]{item});
	}

    /**
     * Adds specified to-do items to the underlying to-do items collection.
     * Notifies that collection has changed.
     * @param items
     */
    public void addTodoItems(TodoItem[] items) {
        for (TodoItem item : items) {
            _todoItems.add(item);
        }

        notifyDataSetChanged();
    }

	/**
     * Removes all to-do items in the given array from the underlying to-do items collection.
     * Notifies that collection has changed.
     * @param items
     */
    public void removeTodoItems(TodoItem[] items) {
        if (items == null || items.length == 0) return;

        for (TodoItem item : items) {
            _todoItems.remove(item);
        }

        notifyDataSetChanged();
    }
}
