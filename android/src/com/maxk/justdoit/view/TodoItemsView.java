package com.maxk.justdoit.view;

import com.maxk.justdoit.model.TodoItem;

/**
 * Represents a view for to-do items.
 * @author MaxK
 */
public interface TodoItemsView {
	void showItems(TodoItem[] items);
    void showItem(TodoItem item, int messageId);
    void revertUpdateTodoItems(TodoItem[] items);
    void showErrorMessage(int messageId);
}
