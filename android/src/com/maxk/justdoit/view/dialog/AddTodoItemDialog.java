package com.maxk.justdoit.view.dialog;

import java.util.Date;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.maxk.justdoit.R;
import com.maxk.justdoit.model.TodoItem;

public class AddTodoItemDialog extends DialogFragment {
	public static final String TAG = DialogFragment.class.getName();
	
	public static interface AddTodoItemDialogCallback {
		void onAddTodoItem(TodoItem item);
	}
	
	private EditText _todoItemTitleView;
	private AddTodoItemDialogCallback _callback;
	
	public void show(FragmentManager fragmentManager) {
		show(fragmentManager, TAG);
	}
	
	public void setCallback(AddTodoItemDialogCallback callback) {
		_callback = callback;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return createDialog(createDialogLayout());
	}
	
	private Dialog createDialog(View contentView) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setView(contentView)
			.setPositiveButton(R.string.todo_items_add, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					addTodoItem(createTodoItem());
				}
			})
			.setNegativeButton(R.string.todo_items_cancel, null);
		
		return builder.create();
	}
	
	private View createDialogLayout() {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View layout = inflater.inflate(R.layout.add_todo_item_dialog_layout, null);
		
		createChildViews(layout);
		
		return layout;
	}
	
	private void createChildViews(View parent) {
		if (parent == null) return;
		
		createTodoItemTitleView(parent);
	}
	
	private void createTodoItemTitleView(View parent) {
		_todoItemTitleView = (EditText)parent.findViewById(R.id.todoItemTitleEditText);
		_todoItemTitleView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				updatePositiveButtonState();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
		});
	}
	
	private void updatePositiveButtonState() {
		updatePositiveButtonState(getPositiveButton(getDialog()));
	}
	
	private void updatePositiveButtonState(Button positiveButton) {
		if (positiveButton == null || _todoItemTitleView == null) return;
		
		positiveButton.setEnabled(_todoItemTitleView.getText().toString().trim().length() > 0);
	}
	
	private Button getPositiveButton(Dialog dialog) {
		if (dialog == null) return null;
		
		return ((AlertDialog)dialog).getButton(Dialog.BUTTON_POSITIVE);
	}
	
	private TodoItem createTodoItem() {
		TodoItem item = new TodoItem();
		item.title = _todoItemTitleView.getText().toString();
		item.createdOn = new Date().getTime();
		
		return item;
	}
	
	private void addTodoItem(TodoItem item) {
		if (_callback != null) _callback.onAddTodoItem(item);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		updatePositiveButtonState();
	}
}
