package com.maxk.justdoit.view.fragment;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.maxk.justdoit.R;
import com.maxk.justdoit.model.TodoItem;
import com.maxk.justdoit.utils.UiUtils;
import com.maxk.justdoit.view.dialog.AddTodoItemDialog;

public class TodoItemsGridFragment extends ItemsGridFragment {
	public static final String TAG = DoneItemsListFragment.class.getName();

    private static final int MENU_ITEM_COMPLETE = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

        showProgressView();
		_itemsController.loadTodoItems();
	}

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.todo_items_list_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_todo_item:
                addTodoItem();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addTodoItem() {
        AddTodoItemDialog dialog = new AddTodoItemDialog();

        dialog.setCallback(new AddTodoItemDialog.AddTodoItemDialogCallback() {
            @Override
            public void onAddTodoItem(TodoItem item) {
                _itemsController.createTodoItem(item);
            }
        });

        dialog.show(getFragmentManager());
    }

    @Override
    public void showItem(TodoItem item, int messageId) {
        UiUtils.showToast(messageId);
        _adapter.addTodoItem(item);
    }

    @Override
    protected boolean createContextualMenu(Menu menu) {
        MenuItem menuItem = menu.add(
                Menu.NONE,
                MENU_ITEM_COMPLETE,
                Menu.NONE,
                R.string.menu_item_complete);

        menuItem.setIcon(R.drawable.ic_menu_done);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        return super.createContextualMenu(menu);
    }

    @Override
    protected boolean onContextualMenuItemSelected(MenuItem menuItem, TodoItem[] selectedItems) {
        switch (menuItem.getItemId()) {
            case MENU_ITEM_COMPLETE:
                completeTodoItems(selectedItems);
                return true;
            default:
                return super.onContextualMenuItemSelected(menuItem, selectedItems);
        }
    }

    private void completeTodoItems(TodoItem[] items) {
        _itemsController.completeTodoItems(items);
        _adapter.removeTodoItems(items);
    }
}
