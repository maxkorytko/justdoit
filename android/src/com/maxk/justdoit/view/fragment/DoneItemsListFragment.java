package com.maxk.justdoit.view.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.maxk.justdoit.R;

public class DoneItemsListFragment extends ItemsListFragment {
	public static final String TAG = DoneItemsListFragment.class.getName();
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setEmptyText(getResources().getString(R.string.todo_items_list_done_empty));
		
		_itemsController.loadDoneItems();
	}
	
	@Override
	public View createRightActionView(Context context, final int position) {
		Button button = new Button(context);
		
		button.setBackground(getActionViewDrawable(R.drawable.ic_action_delete, Color.RED));
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteTodoItem(_adapter.getTodoItem(position));
			}
		});
		
		return button;
	}
	
	@Override
	public View createLeftActionView(Context context, int position) {
		return null;
	}
}
