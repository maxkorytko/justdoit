package com.maxk.justdoit.view.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.TextView;

import com.maxk.justdoit.R;
import com.maxk.justdoit.controller.TodoItemsController;
import com.maxk.justdoit.model.TodoItem;
import com.maxk.justdoit.utils.UiUtils;
import com.maxk.justdoit.view.TodoItemsView;
import com.maxk.justdoit.view.adapter.TodoItemsAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Displays to-do items in a grid.
 * @author MaxK
 */
public class ItemsGridFragment extends Fragment implements TodoItemsView, TodoItemsAdapter.ViewProvider {
    public static final int MENU_ITEM_DELETE = 1;

	protected TodoItemsController _itemsController;
    protected TodoItemsAdapter _adapter;
    private GridView _gridView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.todo_items_grid_layout, container, false);
		
		setupGridView((GridView)view.findViewById(R.id.itemsGridView));
		
		return view;
	}

    private void setupGridView(GridView gridView) {
        if (gridView == null) return;

        gridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        gridView.setMultiChoiceModeListener(new MultiChoiceModeGridViewListener());

        _gridView = gridView;
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		_itemsController = new TodoItemsController(this);
	}

    public void showProgressView() {
        showProgressView(true);
    }

    private void showProgressView(boolean show) {
        getActivity().setProgressBarIndeterminateVisibility(show);
    }

	@Override
	public void showItems(TodoItem[] items) {
		if (items == null) return;

        hideProgressView();
		_gridView.setAdapter(_adapter = new TodoItemsAdapter(items, this));
	}

    public void hideProgressView() {
        showProgressView(false);
    }

    @Override
    public void showItem(TodoItem item, int messageId) {
        // nothing
    }

    @Override
    public void revertUpdateTodoItems(TodoItem[] items) {
        _adapter.addTodoItems(items);
    }

    @Override
    public void showErrorMessage(int messageId) {
        UiUtils.showToast(messageId);
    }

	@Override
	public View getTodoItemView(View convertView, ViewGroup parent, int position) {
		ViewGroup todoItemView = (ViewGroup)convertView;
		
		if (todoItemView == null) {
			todoItemView = (ViewGroup)createTodoItemView(parent);
		}
		
		TextView titleView = (TextView)todoItemView.findViewById(R.id.titleTextView);
		TextView subtitleView = (TextView)todoItemView.findViewById(R.id.subtitleTextView);
		
		final TodoItem item = _adapter.getTodoItem(position);
		if (item != null) {
			titleView.setText(item.title);
			subtitleView.setText(item.formatCreatedOnDate());
		}
		
		return todoItemView;
	}
	
	private View createTodoItemView(ViewGroup root) {
		return LayoutInflater.from(getActivity()).inflate(R.layout.todo_grid_item_layout, root, false);
	}

    private class MultiChoiceModeGridViewListener implements AbsListView.MultiChoiceModeListener {
        private final List<TodoItem> _selectedItems = new ArrayList<TodoItem>();

        @Override
        public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
            final TodoItem item = _adapter.getTodoItem(position);

            if (item != null) {
                if (checked) _selectedItems.add(item);
                else _selectedItems.remove(item);
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            return createContextualMenu(menu);
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            if (onContextualMenuItemSelected(menuItem, _selectedItems.toArray(new TodoItem[0]))) {
                actionMode.finish();
                return true;
            }

            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            // Here you can make any necessary updates to the activity when
            // the CAB is removed. By default, selected items are deselected/unchecked.
        }
    }

    protected boolean createContextualMenu(Menu menu) {
        MenuItem menuItem = menu.add(
                Menu.NONE,
                MENU_ITEM_DELETE,
                Menu.NONE,
                R.string.menu_item_delete);

        menuItem.setIcon(android.R.drawable.ic_menu_delete);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        return true;
    }

    /**
     * This method gets called when a menu item is selected from a contextual action bar.
     * @param menuItem Selected menu item.
     * @param selectedItems A list of selected to-do items.
     * @return true if the action has been handled or false otherwise.
     */
    protected boolean onContextualMenuItemSelected(MenuItem menuItem, TodoItem[] selectedItems) {
        boolean handled = true;

        switch (menuItem.getItemId()) {
            case MENU_ITEM_DELETE:
                deleteTodoItems(selectedItems);
                break;
            default:
                handled = false;
        }

        return handled;
    }

    private void deleteTodoItems(TodoItem[] items) {
        if (items == null || items.length == 0) return;

        _itemsController.deleteTodoItems(items);
        _adapter.removeTodoItems(items);
    }
}
