package com.maxk.justdoit.view.fragment;

import android.app.ListFragment;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.maxk.justdoit.controller.TodoItemsController;
import com.maxk.justdoit.model.TodoItem;
import com.maxk.justdoit.utils.UiUtils;
import com.maxk.justdoit.view.TodoItemsView;
import com.maxk.justdoit.view.TodoListItemView;
import com.maxk.justdoit.view.adapter.TodoItemsAdapter;

/**
 * Displays to-do items in a list.
 * @author MaxK
 *
 */
public abstract class ItemsListFragment extends ListFragment
	implements TodoItemsView, TodoItemsAdapter.ViewProvider, TodoListItemView.ActionViewFactory {
	
	protected TodoItemsController _itemsController;
	protected TodoItemsAdapter _adapter;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		_itemsController = new TodoItemsController(this);
	}
	
	@Override
	public final void showItems(TodoItem[] items) {
		if (items == null) return;
		
		_adapter = new TodoItemsAdapter(items, this);
		setListAdapter(_adapter);
	}

    @Override
    public void showItem(TodoItem item, int messageId) {
        // nothing
    }

    @Override
    public void revertUpdateTodoItems(TodoItem[] items) {
        // nothing
    }

    @Override
    public void showErrorMessage(int messageId) {
        UiUtils.showToast(messageId);
    }

    @Override
	public View getTodoItemView(View convertView, ViewGroup parent, int position) {
		TodoListItemView todoItemView = (TodoListItemView)convertView;
		
		if (todoItemView == null) {
			todoItemView = new TodoListItemView(getActivity(), position, this);
		}
		
		todoItemView.render(_adapter.getTodoItem(position));
		
		return todoItemView;
	}
	
	protected void deleteTodoItem(TodoItem item) {
		if (item == null) return;

        final TodoItem[] items = new TodoItem[]{item};

		_itemsController.deleteTodoItems(items);
		_adapter.removeTodoItems(items);
	}
	
	protected Drawable getActionViewDrawable(int resourceId, int fillColor) {
		Drawable drawable = getResources().getDrawable(resourceId);
		return fillDrawable(drawable, fillColor);
	}
	
	private static Drawable fillDrawable(Drawable drawable, int color) {
		if (drawable == null) return null;
		
		drawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
		
		return drawable;
	}
}
