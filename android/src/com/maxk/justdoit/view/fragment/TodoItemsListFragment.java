package com.maxk.justdoit.view.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.maxk.justdoit.R;
import com.maxk.justdoit.model.TodoItem;
import com.maxk.justdoit.utils.UiUtils;
import com.maxk.justdoit.view.adapter.TodoItemsAdapter;
import com.maxk.justdoit.view.dialog.AddTodoItemDialog;
import com.maxk.justdoit.view.dialog.AddTodoItemDialog.AddTodoItemDialogCallback;

public class TodoItemsListFragment extends ItemsListFragment {
	public static final String TAG = TodoItemsListFragment.class.getName();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setEmptyText(getResources().getString(R.string.todo_items_list_empty));
		
		_itemsController.loadTodoItems();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.todo_items_list_menu, menu);
		
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_add_todo_item:
				addTodoItem();
				return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void addTodoItem() {
		AddTodoItemDialog dialog = new AddTodoItemDialog();
		
		dialog.setCallback(new AddTodoItemDialogCallback() {
			@Override
            public void onAddTodoItem(TodoItem item) {
				_itemsController.createTodoItem(item);
			}
		});
		
		dialog.show(getFragmentManager());
	}

    @Override
    public void showItem(TodoItem item, int messageId) {
        addTodoItem(item);
        UiUtils.showToast(messageId);
    }

    private void addTodoItem(TodoItem item) {
		addTodoItem(item, _adapter);
	}
	
	private void addTodoItem(TodoItem item, TodoItemsAdapter adapter) {
		if (item == null || adapter == null) return;
		
		adapter.addTodoItem(item);
	}
	
	@Override
	public View createRightActionView(Context context, final int position) {
		Button button = new Button(context);
		
		button.setBackground(getActionViewDrawable(R.drawable.ic_action_done, Color.GREEN));
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				completeTodoItem(_adapter.getTodoItem(position));
			}
		});
		
		return button;
	}
	
	private void completeTodoItem(final TodoItem item) {
		if (item == null) return;

        final TodoItem[] items = new TodoItem[]{item};

		_itemsController.completeTodoItems(items);
        _adapter.removeTodoItems(items);
    }

    @Override
    public void revertUpdateTodoItems(TodoItem[] items) {
        _adapter.addTodoItems(items);
    }

	@Override
	public View createLeftActionView(Context context, final int position) {
		Button button = new Button(context);
		
		button.setBackground(getActionViewDrawable(R.drawable.ic_action_delete, Color.RED));
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteTodoItem(_adapter.getTodoItem(position));
			}
		});
		
		return button;
	}
}
