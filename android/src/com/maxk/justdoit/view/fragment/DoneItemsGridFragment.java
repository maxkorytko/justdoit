package com.maxk.justdoit.view.fragment;

import android.os.Bundle;

public class DoneItemsGridFragment extends ItemsGridFragment {
    public static final String TAG = DoneItemsListFragment.class.getName();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        _itemsController.loadDoneItems();
    }
}
