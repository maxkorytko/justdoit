package com.maxk.justdoit.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.maxk.justdoit.model.TodoItem;
import com.maxk.justdoit.model.UpdateTodoItemResult;

import java.io.UnsupportedEncodingException;

/**
 * Encapsulates a request to an API endpoint that requires a JSON object in the request body.
 * Created by maxk on 14-12-08.
 */
public class JustDoItApiJsonRequest<T> extends JustDoItApiRequest<T> {
    private static final String PROTOCOL_CHARSET = "utf-8";

    private final String _requestBody;

    /**
     * Creates a request to the update item end-point.
     * The item must exist on the back-end.
     * @param items
     * @param listener
     * @param errorListener
     * @return
     */
    public static JustDoItApiJsonRequest<UpdateTodoItemResult> updateTodoItemsRequest(TodoItem[] items,
                                                                                      Response.Listener<UpdateTodoItemResult> listener,
                                                                                      Response.ErrorListener errorListener) {
        if (items == null) throw new IllegalArgumentException("items");

        JustDoItApiJsonRequest<UpdateTodoItemResult> request = new JustDoItApiJsonRequest<UpdateTodoItemResult>(
                Request.Method.PUT,
                JustDoItApiRequestUrl.toEndpoint("items/update").toString(),
                createGsonObject().toJson(items),
                UpdateTodoItemResult.class,
                listener,
                errorListener);

        return request;
    }

    public JustDoItApiJsonRequest(int method, String url, String jsonBody, Class<T> clazz,
                                  Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, clazz, listener, errorListener);

        _requestBody = jsonBody;
    }

    @Override
    public String getBodyContentType() {
        return String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    }

    @Override
    public byte[] getBody() {
        try {
            return _requestBody == null ? null : _requestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    _requestBody, PROTOCOL_CHARSET);
            return null;
        }
    }
}
