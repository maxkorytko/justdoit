package com.maxk.justdoit.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Encapsulates Volley request queue.
 * @author MaxK
 * 
 */
public class JustDoItApiRequestQueue {
	private static JustDoItApiRequestQueue _instance;
	private final Context _context;
	private final RequestQueue _requestQueue;
	
	/**
	 * Returns a single instance of this class.
	 */
	public static synchronized JustDoItApiRequestQueue getInstance(Context context) {
		if (_instance == null) _instance = new JustDoItApiRequestQueue(context);
		return _instance;
	}
	
	private JustDoItApiRequestQueue(Context context) {
		if (context == null) throw new IllegalArgumentException("context");
		
		_context = context.getApplicationContext();
		_requestQueue = Volley.newRequestQueue(_context);
	}
	
	/**
	 * Adds the given request to the queue.
	 */
	public <T> void addRequest(Request<T> request) {
		get().add(request);
	}
	
	/**
	 * Returns Volley request queue.
	 */
	public RequestQueue get() {
		return _requestQueue;
	}
}
