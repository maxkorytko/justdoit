package com.maxk.justdoit.api;

import android.net.Uri;

/**
 * Helper class for creating a URL to and API end-point.
 * @author maxk
 *
 */
public final class JustDoItApiRequestUrl {
	private final Uri.Builder _uriBuilder;
	
	/**
	 * Creates a new instance of the class and initializes it with the specified end-point.
	 * @param endpoint
	 * @return
	 */
	public static JustDoItApiRequestUrl toEndpoint(String endpoint) {
		if (endpoint == null) throw new IllegalArgumentException("endpoint");
		
		return new JustDoItApiRequestUrl(Uri.parse(endpoint));
	}
	
	/**
	 * The user of the class must call the static method to create an instance of the class.
	 * @param uri
	 */
	private JustDoItApiRequestUrl(Uri uri) {
		if (uri == null) throw new IllegalArgumentException("uri");
		
		_uriBuilder = uri.buildUpon();
		init(uri);
	}
	
	private void init(Uri uri) {
		_uriBuilder
			.scheme("http")
			.authority("10.0.2.2:3000") // localhost in Android Emulator
			.path("api/1.0")
			.appendPath(uri.getPath());
	}
	
	/**
	 * Sets the query string.
	 * Overwrites a previously set query string.
	 * @param queryString
	 * @return
	 */
	public JustDoItApiRequestUrl withQueryString(String queryString) {
		_uriBuilder.query(queryString);
		return this;
	}
	/**
	 * Appends the provided path segment to the URL.
	 * @param path
	 * @return
	 */
	public JustDoItApiRequestUrl appendPath(String path) {
		_uriBuilder.appendPath(path);
		return this;
	}
	
	@Override
	public String toString() {
		return Uri.decode(_uriBuilder.build().toString());
	}
}
