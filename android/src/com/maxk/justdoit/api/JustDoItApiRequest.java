package com.maxk.justdoit.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.maxk.justdoit.application.JustDoItApplication;
import com.maxk.justdoit.model.CreateTodoItemResult;
import com.maxk.justdoit.model.TodoItem;

/**
 * Encapsulates a request to an API end-point.
 * Expects the end-point to return JSON.
 * Note that this class uses GSON to parse the end-point response.
 * For this reason, the type you provide in place of <T> must be compatible with end-point response.
 * This is because GSON maps JSON to Java objects.
 * For more details, see the documentation on GSON.
 * @author MaxK
 *  
 */
public class JustDoItApiRequest<T> extends Request<T> {
	private final Listener<T> _listener;
	private final Class<T> _resultObjectClass;
	private final Gson _gson = createGsonObject();
	private final Map<String, String> _postParams = new HashMap<String, String>();
	
	protected static Gson createGsonObject() {
		return new GsonBuilder()
			.registerTypeAdapter(long.class, new LongTypeAdapter())
			.registerTypeAdapter(Long.class, new LongTypeAdapter()).create();
	}
	
	/**
	 * Adapter for type Long.
	 * Every time GSON sees a value that it thinks is of type Long, this adapter will be used to parse it.
	 * The adapter is also used for converting Long to JSON.
	 * @author MaxK
	 *
	 */
	private static class LongTypeAdapter extends TypeAdapter<Number> {
		@Override
		public void write(JsonWriter out, Number value) throws IOException {
			out.value(value);
		}
		
		@Override
		public Number read(JsonReader in) throws IOException {
			if (in.peek() == JsonToken.NULL) {
				in.nextNull();
				return null;
			}
			
			try {
				String result = in.nextString();
				if ("".equals(result)) return null;
				return Long.parseLong(result);
			} catch (NumberFormatException e) {
				throw new JsonSyntaxException(e);
			}
		}
	}
	
	/**
	 * Creates an items list API end-point request.
	 * @param filter
	 * @param listener
	 * @param errorListener
	 * @return
	 */
	public static JustDoItApiRequest<TodoItem[]> itemsListRequest(String filter, Listener<TodoItem[]> listener,
			ErrorListener errorListener) {
		return new JustDoItApiRequest<TodoItem[]>(
				Request.Method.GET,
				JustDoItApiRequestUrl.toEndpoint("items").appendPath(filter).toString(),
				TodoItem[].class,
				listener,
				errorListener);
	}
	
	/**
	 * Creates a request to the create item end-point.
	 * Use this method if you need to create a new item on the back-end.
	 * @param item
	 * @param listener
	 * @param errorListener
	 * @return
	 */
	public static JustDoItApiRequest<CreateTodoItemResult> createTodoItemRequest(TodoItem item,
                                                                                 Listener<CreateTodoItemResult> listener,
                                                                                 ErrorListener errorListener) {
		if (item == null) throw new IllegalArgumentException("item");
		
		JustDoItApiRequest<CreateTodoItemResult> saveRequest = new JustDoItApiRequest<CreateTodoItemResult>(
				Request.Method.POST,
				JustDoItApiRequestUrl.toEndpoint("items/create").toString(),
                CreateTodoItemResult.class,
				listener,
				errorListener);
		
		saveRequest.addPostParam("title", item.title);
		saveRequest.addPostParam("createdOn", String.valueOf(item.createdOn));
		
		return saveRequest;
	}
	
	/**
	 * Creates a request to the items delete end-point.
	 * @param items
	 * @param listener
	 * @param errorListener
	 * @return
	 */
	public static JustDoItApiRequest<Void> deleteTodoItemsRequest(TodoItem[] items,
			Listener<Void> listener,
			ErrorListener errorListener) {
		if (items == null) throw new IllegalArgumentException("item");
		
		return new JustDoItApiRequest<Void>(
				Request.Method.DELETE,
				JustDoItApiRequestUrl.toEndpoint("/items/delete").toString(),
				Void.class,
				listener,
				errorListener);
	}
	
	public JustDoItApiRequest(int method, String url, Class<T> clazz,
			Listener<T> listener, ErrorListener errorListener) {
		super(method, url, errorListener);
		
		if (url == null) throw new IllegalArgumentException("url");
		if (listener == null) throw new IllegalArgumentException("listener");
		
		_resultObjectClass = clazz;
		_listener = listener;
	}

    /**
	 * Adds a parameter to the post map.
	 * @param name
	 * @param value
	 */
	public void addPostParam(String name, String value) {
		_postParams.put(name, value);
	}
	
	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
        return _postParams;
    }
	
	@Override
	protected void deliverResponse(T response) {
		_listener.onResponse(response);
	}

	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse response) {
		try {
			String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			return Response.success(parseJson(json), HttpHeaderParser.parseCacheHeaders(response));
		} catch (Exception ex) {
			return Response.error(new ParseError(ex));
		}
	}
	
	private T parseJson(String json) {
		return _gson.fromJson(json, _resultObjectClass);
	}
	
	/**
	 * Enqueues the request.
	 */
	public final void execute() {
		JustDoItApiRequestQueue.getInstance(JustDoItApplication.getContext()).addRequest(this);
	}
}
