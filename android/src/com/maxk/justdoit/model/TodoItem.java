package com.maxk.justdoit.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Represents a to-do item.
 * @author MaxK
 * 
 */
public class TodoItem {
	public static interface RequestStatusListener<T> {
		void onRequestSuccess(T result);
		void onRequestFail();
	}
	
	public String id;
    public String title;
	public boolean done;
	public long createdOn;
	
	public TodoItem() { 
		
	}
	
	/**
	 * Formats the date the item was created on using default format string.
	 */
	public String formatCreatedOnDate() {
		return String.format("%s at %s", formatCreatedOnDate("MMM d, y"), formatCreatedOnDate("K:mm a"));
	}
	
	/**
	 * Formats the date the item was created on using the provided format string.
	 */
	public String formatCreatedOnDate(String formatString) {
		return formatDate(new Date(createdOn), formatString);
	}
	
	private static String formatDate(Date date, String formatString) {
		if (date == null || formatString == null) return "";
		
		return new SimpleDateFormat(formatString, Locale.US).format(date);
	}
}
