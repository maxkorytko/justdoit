package com.maxk.justdoit.model;

public class CreateTodoItemResult {
	public String error;
	public TodoItem item;
}
