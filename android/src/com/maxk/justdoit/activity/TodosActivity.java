package com.maxk.justdoit.activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

import com.maxk.justdoit.view.tabs.factory.TabsFactory;

/**
 * Main activity for the application
 * @author MaxK
 *
 */
public class TodosActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setupTabs(getActionBar());
	}
	
	private void setupTabs(ActionBar actionBar) {
		if (actionBar == null) return;
		
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		addTabs(actionBar, new TabsFactory(this).makeTabsForTodosActivity());
	}
	
	private void addTabs(ActionBar actionBar, Tab[] tabs) {
		if (actionBar == null || tabs == null) return;
		
		for (Tab tab : tabs) {
			actionBar.addTab(tab);
		}
	}
}
