# JustDoIt

Just do it is a very simple todo list application. I started building this app in order to gain some familiarity with Node JS and explore new Android SDKs.

## JustDoIt comprises of two parts:

* backend
* frontend

## The Backend

The backend is implemented using Node JS, Express, and Mongo DB.

## The Frontend

Currently, the only available frontend piece is an Android app.

## Please note, that this is a work in progress.

Some features have not been implemented yet.
Feel free to contribute to the project, so that we can all learn from each other. 
